angular.module('zampieriAdmin', ['ionic', 'zampieriAdmin.controllers', 'ngMask', 'zampieriAdmin.services', 'ionic-material', 'ngScrollGlue', 'chart.js'])

.run(function($ionicPlatform, $rootScope, popup, $state) {
    $ionicPlatform.ready(function() {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
    var isWebView = ionic.Platform.isWebView();


})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.backButton.text('').icon('ion-chevron-left');
    
    $stateProvider

    .state('login', {
        url: '/login',
        templateUrl: 'templates/deslogado/login/login.html',
        controller: 'loginCtrl',
        onEnter: function($state, $rootScope, popup) {
            if (typeof(window.localStorage.dataLogin) != "undefined") {
                $state.go('app.dash');
            }
        }
    })
    .state('app', {
        url: '/admin',
        abstract: true,
        templateUrl: 'templates/logado/menu.html',
        controller: 'logadoCtrl',
        onEnter: function($state, $rootScope, popup) {
            if (typeof(window.localStorage.dataLogin) == "undefined") {
                popup("Opsss", "Parece que você não está logado");
                $state.go('login');
            }
        }
    })


    .state('app.dash', {
        url: '/dashboard',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/dashboard/dashboard.html',
                controller: 'dashboardCtrl'
            }
        }
    })



    .state('app.pagamentos', {
        url: '/pagamentos',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/pagamentos/pagamentos.html',
                controller: 'pagamentosCtrl'
            }
        }
    })
    .state('app.pagamentoEspecifico', {
        url: '/pagamentoEspecifico',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/pagamentos/pagamentoEspecifico.html',
                controller: 'pagamentosCtrl'
            }
        }
    })



    .state('app.indicacoesinquilinos', {
        url: '/indicacoesinquilinos',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/indicacoesinquilino/indicacoes.html',
                controller: 'indicacoesInquilinosCtrl'
            }
        }
    })
    .state('app.indicacaoEspecifica', {
        url: '/indicacaoEspecifica',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/indicacoesinquilino/indicacaoEspecifica.html',
                controller: 'indicacoesInquilinosCtrl'
            }
        }
    })



    .state('app.indicacoesimoveis', {
        url: '/indicacoesimoveis',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/indicacoesimoveis/indicacoes.html',
                controller: 'indicacoesImoveisCtrl'
            }
        }
    })
    .state('app.indicacaoEspecificaImoveis', {
        url: '/indicacaoEspecificaImoveis',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/indicacoesimoveis/indicacaoEspecifica.html',
                controller: 'indicacoesImoveisCtrl'
            }
        }
    })



    .state('app.meusdados', {
        url: '/meusdados',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/meusdados/meusdados.html',
                controller: 'meusDadosCtrl'
            }
        }
    })
    
    .state('app.graficos', {
        url: '/graficos',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/graficos/graficos.html',
                controller: 'graficosCtrl'
            }
        }
    })

    .state('app.suportechat', {
        url: '/suportechat',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/suportechat/suportechat.html',
                controller: 'suportechatCtrl'
            }
        }
    })
    .state('app.suportechatEspecifico', {
        url: '/suportechatEspecifico',
        views: {
            'menuContent': {
                templateUrl: 'templates/logado/suportechat/suportechatEspecifico.html',
                controller: 'suportechatCtrl'
            }
        }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');
});