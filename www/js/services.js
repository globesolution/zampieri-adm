angular.module('zampieriAdmin.services', []) //adicione o modulo a sua app

.constant('zampieri', {
        "url": "http://savo.zampieriimoveis.com.br/zampieri-api/public"
    })
.factory('requisicao', function($http, zampieri) {

    return function(url, success, error, method, params) {

        $http({
                'url': zampieri.url + url,
                'method': method,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                transformRequest: function(obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                'data': params
            })
            .then(
                function successCallback(response) {
                    if (typeof(success) == "function")
                        success(response);
                },
                function errorCallback(response) {
                    if (typeof(error) == "function")
                        error(response);
                }
            );
    }
})
.factory('popup', function($ionicPopup) {
    return function(titulo, msg) {
        $ionicPopup.alert({ //veja a popup que vc vai usar, se for usar, substiua essa parte por algo util a vc
            title: titulo,
            template: msg,
            okText: 'Prosseguir'
        });
    }
})
.factory('api', function($http, requisicao) {
// qunado for fazer o request no controller, vc adc o nome api como parametro e chama essas funções
//exemplo .controller('exempleCtrl', function($scope, api, popup) { api.login(json, function successCallback(), function erroCallback()); });

    return {
        login: function(loginData, success, error) { //login service
            requisicao('/api/auth', success, error, "POST", loginData);
        },
        changePassword: function(passwordData, success, error){ //  change password
            requisicao('/api/usuario/changepassword', success, error, "POST", passwordData);
        },
        getListPagamentos: function(data, success, error){ //cadastro user service
            requisicao('/api/admin/list/pagamentos', success, error, "POST", data);
        }, 
        savePagamento: function(data, success, error){
            requisicao('/api/admin/save/pagamento', success, error, "POST", data);
        }, 
        getListIndicacoes: function(data, success, error){ //cadastro user service
            requisicao('/api/admin/list/indicacoes', success, error, "POST", data);
        },
        saveIndicacao: function(data, success, error){
            requisicao('/api/admin/save/indicacao', success, error, "POST", data);
        },
        getNumberIndicacoes: function(data, success, error){
            requisicao('/api/admin/number/indicacoes', success, error, "POST", data);
        },
        sendMsgNovidade: function(data, success, error){
            requisicao('/api/admin/msg/novidades', success, error, "POST", data);
        },
        getMsgsToList: function(data, success, error){
            requisicao('/api/admin/getlist/msg', success, error, "POST", data);
        },
        getMsgsBySender: function(data, success, error){
            requisicao('/api/admin/getbysender/msg', success, error, "POST", data);
        },
        sendNewMsg: function(data, success, error){
            requisicao('/api/admin/send/msg', success, error, "POST", data);
        }
    };
});