angular.module('zampieriAdmin.controllers', [])







.controller('loginCtrl', function($scope, $rootScope, $state, api, popup) {

  $scope.loginData = {};


  $scope.doLogin = function() {
    if ($scope.loginData.email == null || $scope.loginData.senha == null) {
            popup("Opsss", "Parece que você esqueceu de algo.");
            return;
        } else {
            api.login($scope.loginData,
                function(res) {
                    if (res.data.status == 200) {
                      if(res.data.usuario.type_user == 2){
                        $rootScope.dataLogin = res.data;
                        window.localStorage.dataLogin = JSON.stringify(res.data);
                        console.log($rootScope.dataLogin);
                        $state.go('app.dash');
                      }else{
                        popup("Opsss", "Parece que você não é admin, tente logar pelo aplicativo de usuários comuns.");
                      }
                    } else
                        popup("Opsss", res.data.msg);
                },
                function(res) {
                    popup("Opsss", "Parece que ocorreu um erro.");
                }
            );
        }
  };
})











.controller('logadoCtrl', function($scope, $rootScope, api) {
  $scope.logout = function(){
    $rootScope.dataLogin = undefined;
    window.localStorage.removeItem("dataLogin");
  } 

  $scope.getNumberIndicacoes = function(){
    data = {
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha
    };
    api.getNumberIndicacoes(data,
      function(res){
        if(res.data.status == 200){
          $scope.imoveis = res.data.number_indicacao.imoveis;
          $scope.inquilinos = res.data.number_indicacao.inquilinos;
          $scope.pagamentos = res.data.pagamentos;
        }
        else
          popup("Opsss", res.data.msg);
      }, 
      function(res){
        console.log(res);
      });
  }
})











.controller('dashboardCtrl', function($scope, $rootScope, $stateParams, $ionicPopup, api, popup) {
  $scope.setDate = function(data){
    resgistro = new Date(data);
    return resgistro;
  }
  $scope.retornaIcon = function(tipo){
    if (tipo == 1) {
      return "ion-home";
    }
    if (tipo == 2){
      return "ion-person";
    }
  }
  $scope.retornaStatus = function(status){
    if(status == 1){
      return "Em aberto";
    }
    if (status == 2) {
      return "Concluído";
    }
    if(status == 3){
      return "Cancelado";
    }
  }
  $scope.labels = ["Indicações Concluídas", "Indicações Em aberto", "Indicações Canceladas"];
  $scope.data = [
    (JSON.parse(window.localStorage.dataLogin)).dash.indicacoes.concluidas, 
    (JSON.parse(window.localStorage.dataLogin)).dash.indicacoes.abertas, 
    (JSON.parse(window.localStorage.dataLogin)).dash.indicacoes.canceladas
  ];

  $scope.dataIndicacoes = (JSON.parse(window.localStorage.dataLogin)).dash.indicacoes.lista;

  $scope.msgData = {};
  $scope.msgData.msgnovidade = '';
  $scope.msg = (JSON.parse(window.localStorage.dataLogin)).msgnovidade ? (JSON.parse(window.localStorage.dataLogin)).msgnovidade : 'Nenhuma mensagem cadastrada.';
  $scope.validate = function(){
    console.log($scope.msgData.msgnovidade.length);
    if($scope.msgData.msgnovidade.length == 0)
      msgpopup = 'A mensagem está vazia, mensagem vazia significa que você está deixando o aplicativo sem a mensagem, você tem certeza que deseja fazer isso?';
    else
      msgpopup = 'Essa alteração irar alterar a mensagem exibida no aplicativo, você tem certeza que deseja fazer isso?';
    var confirmPopup = $ionicPopup.confirm({
     title: 'Você tem certeza?',
     template: msgpopup,
     cancelText:'Não',
     okText:'Sim'
   });

   confirmPopup.then(
      function(res) {
        if(res){
          sendNewMsg();
        }else{
          console.log('not send request');
        }
      }
    );
  }

  sendNewMsg = function(){
    data = {
          usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
          senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
          msgData:$scope.msgData.msgnovidade
        }
    api.sendMsgNovidade(data, 
    function(res){
      if(res.data.status == 200){
          $scope.msg = $scope.msgData.msgnovidade;
          window.localStorage.dataLogin.msgnovidade = $scope.msgData.msgnovidade;
          $scope.msgData.msgnovidade = '';
          popup("Sucesso", res.data.msg);
        }
        else
          popup("Opsss", res.data.msg);
      }, 
      function(res){
        console.log(res);
      }
    );
  }
})











.controller('pagamentosCtrl', function($scope, $rootScope, $stateParams, api, popup, $state, $window) {
  $scope.pagamentos = [];
  $scope.msgNoPagamentos = false;

  $scope.setDate = function(data){
    resgistro = new Date(data);
    return resgistro;
  }
 
  $scope.getPagamentosByType = function(type){
    $scope.pagamentos = [];
    data = {
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
      statusPagamento:type
    }
    api.getListPagamentos(data, 
      function(res){
        if(res.data.status == 200){
          $scope.pagamentos = res.data.pagamentos;
          if($scope.pagamentos.length == 0)
            $scope.msgNoPagamentos = true;
          else
            $scope.msgNoPagamentos = false;

        }
        else
          popup("Opsss", res.data.msg);
      }, 
      function(res){
        console.log(res);
      });

  }
  $scope.passPagamentoEspecifico = function(pagamento){
    $rootScope.pagamentoEspecifico = pagamento;
    $state.go('app.pagamentoEspecifico');
  }


  $scope.converteBox = function(){
    console.log($rootScope.pagamentoEspecifico);
    if($rootScope.pagamentoEspecifico.status == 1)
      $scope.pago = false;
    else
      $scope.pago = true;
  }

  $scope.savePagamento = function(pagamento, pg){
    pgmt = {
      id_pagamento:pagamento.id,
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
      statusPagamento:(pg+1)
    }
    api.savePagamento(pgmt,
      function(res) {
        if (res.data.status == 200) {

          $state.go('app.pagamentos',  {}, {reload: true});
          $window.location.reload(true);
        }
        else
          popup("Opsss", res.data.msg);
           
        },
      function(res) {
        popup("Opsss", "Parece que ocorreu um erro.");
      }
    );
  }
  $scope.setIcon = function(status){
  if (status == 1) {
    return "ion-minus-round";
  }
  if (status == 2) {
    return "ion-checkmark-round";
  }
}

})
















.controller('usuariosCtrl', function($scope, $rootScope, $stateParams) {
})






















.controller('indicacoesInquilinosCtrl', function($scope, $rootScope, $state, $stateParams, api, popup, $window) {
  $scope.indicacoes = [];
  $scope.msgNoIndicacoes = false;
 
   $scope.setDate = function(data){
    resgistro = new Date(data);
    return resgistro;
  }

  $scope.getIndicacoesByType = function(type){
    $scope.indicacoes = [];
    data = {
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
      statusIndicacao:type,
      tipo_indicacao:1
    }
    api.getListIndicacoes(data, 
      function(res){
        if(res.data.status == 200){
          $scope.indicacoes = res.data.indicacoes;
          if($scope.indicacoes.length == 0)
            $scope.msgNoIndicacoes = true;
          else
            $scope.msgNoIndicacoes = false;

        }
        else
          popup("Opsss", res.data.msg);
      }, 
      function(res){
        console.log(res);
      });
  }

  $scope.passIndicacaoEspecifica = function(indicacao){
    $rootScope.indicacaoEspecifica = indicacao;
    $state.go('app.indicacaoEspecifica');
  }
 $scope.saveIndicacao = function(indicacao){

    indicacao = {
      id_indicacao:indicacao.id,
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
      statusIndicacao:indicacao.status,
      tipo_indicacao:1,
      creditar:indicacao.creditar
    }
    api.saveIndicacao(indicacao,
      function(res) {
        if (res.data.status == 200) {
          $state.go('app.indicacoesinquilinos',  {}, {reload: true});
          $window.location.reload(true);
        }
        else
          popup("Opsss", res.data.msg);
           
        },
      function(res) {
        popup("Opsss", "Parece que ocorreu um erro.");
      }
    );
  }
   $scope.setIcon = function(status){
  if (status == 1) {
    return "ion-minus-round";
  }
  if (status == 2) {
    return "ion-checkmark-round";
  }
  if (status == 3) {
    return "ion-close-round";
  }
}
})













.controller('indicacoesImoveisCtrl', function($scope, $rootScope, $state, $stateParams, api, popup, $window) {
  $scope.indicacoes = [];
  $scope.msgNoIndicacoes = false;
  $scope.creditar = null;

  $scope.setDate = function(data){
    resgistro = new Date(data);
    return resgistro;
  }
 
  $scope.changeCreditar = function(){
    if($rootScope.typeIndicacao == 2){
      $scope.creditar = true;
    }else{
      $scope.creditar = false;
    }


  console.log($scope.creditar)
  }
  $scope.getIndicacoesByType = function(type){
    $scope.indicacoes = [];
    data = {
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
      statusIndicacao:type,
      tipo_indicacao:2
    }
    
    api.getListIndicacoes(data, 
      function(res){
        if(res.data.status == 200){
          $scope.indicacoes = res.data.indicacoes;
          if($scope.indicacoes.length == 0)
            $scope.msgNoIndicacoes = true;
          else
            $scope.msgNoIndicacoes = false;

        }
        else
          popup("Opsss", res.data.msg);
      }, 
      function(res){
        console.log(res);
      });
  }

  $scope.passIndicacaoEspecifica = function(indicacao){
    $rootScope.indicacaoEspecifica = indicacao;
    $rootScope.typeIndicacao = indicacao.status;
    $state.go('app.indicacaoEspecificaImoveis');
  }
 $scope.saveIndicacao = function(indicacao){
  if(typeof(indicacao.creditar) == "undefined"){
    indicacao.creditar = false
  }

    indicacao = {
      id_indicacao:indicacao.id,
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
      statusIndicacao:indicacao.status,
      tipo_indicacao:2,
      creditar: indicacao.creditar
    }
    api.saveIndicacao(indicacao,
      function(res) {
        if (res.data.status == 200) {
          $state.go('app.indicacoesimoveis',  {}, {reload: true});//parei aqui
          $window.location.reload(true);
        }
        else
          popup("Opsss", res.data.msg);
           
        },
      function(res) {
        popup("Opsss", "Parece que ocorreu um erro.");
      }
    );
  }
  $scope.setIcon = function(status){
  if (status == 1) {
    return "ion-minus-round";
  }
  if (status == 2) {
    return "ion-checkmark-round";
  }
  if (status == 3) {
    return "ion-close-round";
  }
}


})











.controller('graficosCtrl', function($scope, $rootScope, $stateParams) {
})













.controller('suportechatCtrl', function($scope, $rootScope, $stateParams, api, popup, $state, $window) {
  $scope.glued=true;
  $scope.tickets = [];
  $scope.msgNoTickets = false;
  $scope.chat = [];
  $scope.msgData = {};
  $scope.msgData.msg = '';

 
  $scope.setDate = function(data){
    resgistro = new Date(data);
    return resgistro;
  } 
 
  $scope.getMsgByType = function(){
    $scope.tickets = [];
    data = {
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
    }
    api.getMsgsToList(data, 
      function(res){
        if(res.data.status == 200){
          $scope.tickets = res.data.tickets;
          if($scope.tickets.length == 0)
            $scope.msgNoTickets = true;
          else
            $scope.msgNoTickets = false;

        }
        else
          popup("Opsss", res.data.msg);
      }, 
      function(res){
        console.log(res);
      });

  }
  $scope.passToTicketEspecifico = function(ticket){
    $rootScope.ticketEspecifico = ticket;
    $state.go('app.suportechatEspecifico');
  }

  $scope.changeName = function(type){
    if(type == 1)
      return 'Cliente';
    else
      return 'Suporte';
  }

  $scope.getMsgsBySender = function(){
    data = {
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
      enviador_id:$rootScope.ticketEspecifico.enviador_id
    }
    api.getMsgsBySender(data, 
      function(res){
        if(res.data.status == 200){
          $scope.chat = res.data.chat;
        }
        else
          popup("Opsss", res.data.msg);
      }, 
      function(res){
        console.log(res);
      });
  }

  $scope.sendNewMsg = function(){
    data = {
      usuario_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      senha:(JSON.parse(window.localStorage.dataLogin)).usuario.senha,
      enviador_id:(JSON.parse(window.localStorage.dataLogin)).usuario.id,
      receptor_id:$rootScope.ticketEspecifico.enviador_id,
      msg:$scope.msgData.msg
    }
    api.sendNewMsg(data,
      function(res) {
        if (res.data.status == 200) {
          $scope.chat.push({msg:$scope.msgData.msg, tipo_user:2});
          $scope.msgData.msg = '';

        }
        else
          popup("Opsss", res.data.msg);
           
        },
      function(res) {
        popup("Opsss", "Parece que ocorreu um erro.");
      }
    );
  }

})



.controller('meusDadosCtrl', function($scope, $state, $rootScope, api, popup) {

    $scope.passwordData = {};
    $scope.newPassword = function(){

        $scope.passwordData.usuario_id = (JSON.parse(window.localStorage.dataLogin)).usuario.id;
        api.changePassword($scope.passwordData,
            function(res) {
                if (res.data.status == 200) {
                    console.log(res.data);
                    popup("Sucesso", res.data.msg);
                    $scope.passwordData = {};

                } else {
                    console.log('erro');
                    popup("Opss...", res.data.msg);
                }
            },
            function(res) {
                console.log(res);
            }
        );
    } 
});
